# Nix flake for KDE software

## Introduction

This repository packages KDE software for Nix.
Nix is a package manager available for many Linux distributions.
It handles packaging, setting up a development environment,
testing and deploying.

The file [flake.nix](flake.nix) contains instructions for

- running stable and nightly KDE software,
- setting up a build environment with `nix develop`
- running tests with `nix check`
- using Plasma as a desktop
- using the binary cache for quick installation
- running jobs on a [CI server](https://hydra.kde.broncode.org/jobset/kde-nix-flake/kde)

_**Note:** [nix flake](https://nixos.wiki/wiki/Flakes) is a,
widely used,
experimental feature of Nix._


## Run a KDE program

First make sure Nix [is installed](#installing-nix).
Then run

```
FLAKE=git+https://invent.kde.org/vandenoever/kde-nix-flake
nix run $FLAKE#kf6-qt6.konsole
```

This command runs a recent KF6 version of Konsole.
It retrieves all requirements and places them in `/nix/store`.

Other packages can be found with `nix flake show` or by tab-completion.

_**Note:** The command will ask you if you trust the binary cache that it provides.
If you deny, it will compile the package locally._


## Develop a KDE program

The Nix command `nix develop` opens a shell where all requirements are present.
The shell contains commands to configure, compile and test a package
with no need to remember specific arguments to the used tools.

Below is a short session where the konsole repository is cloned
and the shell functions `cmakeConfigurePhase`, `buildPhase`, and `installPhase`
are used to obtain binary files.
After a file edit, `buildPhase` or `installPhase` can be run again
to get a fresh executable.


```
$ cd kde-nix-flake
$ nix develop .#kf6-qt6.konsole
$ git clone git@invent.kde.org:utilities/konsole
...
$ cd konsole
[konsole]$ cmakeConfigurePhase
fixing cmake files...
cmake flags: -DCMAKE_...
...
-- Generating done
[konsole/build]$ buildPhase
build flags: -j24 SHELL=/run/current-system/sw/bin/bash
[  0%] Generating mo...
...
[100%] Built target konsole_sshmanagerplugin
[konsole/build]$ bin/konsole
[konsole/build]$ installPhase
install flags: -j24 SHELL=/run/current-system/sw/bin/bash install
...
-- Installing: .../outputs/out/share/zsh/site-functions/_konsole
[konsole/build]$ .../outputs/out/bin/konsole
```

Additional phases can be found with `declare -F | grep Phase`.

## Run nightly Plasma in a virtual machine

The commands below create a VM and start it. The username and password are 'tester' and 'test' (see `flake.nix`).

```
nix shell nixpkgs#nixos-rebuild
nixos-rebuild build-vm --flake .#kf6-qt6
result/bin/run-nixos-vm
```

_**Note:** Any help to make the Plasma session in the VM work better is appreciated._

## What are Nix and Nix flakes?

Nix is a package manager that can be used in many distributions.
A `nix flake` is a collection of build recipes.
In `nix`, these are called derivations.
It is defined in a file called `flake.nix`.
That is accompanied by a file called `flake.lock` which fixes all dependencies to exact version.
The versions are checked by a checksum.

KDE is software is packaged in the main repository of Nix, called nixpgs.
Nix flakes are used to provide more recent or altered versions of packages that
come with nixpkgs.

The flake in this repostory points to versions from the git repositories of KDE.
This makes it ideal for daily builds.
All builds are cached.
When a package and its dependencies have not changed, the software does not need to be rebuilt.

## What version of the software is packaged?

Software versions in KDE are grouped into *layers*.
Currently the layers are `stable-kf5-qt5`, `kf5-qt5` and `kf6-qt6`.
The master branch of this Nix flake packages all packages from all layers.
This includes software from KDE Frameworks, Plasma and KDE Gear.

## Synchronizing with Nixpkgs

Parts of this flake are adapted from version 23.05 of Nixpkgs.
Most of the .nix files in this repository were forked from the directories

 * `pkgs/applications/kde`
 * `pkgs/desktops/plasma-5`
 * `pkgs/development/libraries/kde-frameworks`

of the Nixpkgs repository.

When a package fails to build it is recommend to look in the Nixpkgs repository to see what the status there is.

<a id="installing-nix"></a>

## Installing Nix

The package manager Nix is included in <a href="https://repology.org/project/nix/information#Repositories">many Linux distributions</a>.
Alternatively, the official <a href="https://nixos.org/download.html#nix-install-linux">installation script</a> can be used.

Version 2.7 or newer is required.

Here are instructions on installing `nix` on Debian:

1) install nix (>= 2.7.0)
```bash
apt install nix
```
2) edit /etc/nix/nix.conf to contain
```
experimental-features = nix-command flakes
trusted-users = user1 user2
```
3) add the users to the group `nix-users` in `/etc/group`, e.g.
```
nix-users:x:994:user1,user2
```
4) restart the nix daemon
```bash
sudo systemctl restart nix-daemon
```
5) Check that it works:
```
$ nix run nixpkgs#hello
Hello world!
```


## Quick test

To quickly test this flake by compiling and running `kcalc`, run
```
git clone https://invent.kde.org/vandenoever/kde-nix-flake.git
cd kde-nix-flake
nix run .#kf6-qt6.kcalc
```

## Updating the packages

A script to update the flake to the current versions of all packages is included and can be run as
```bash
git clone https://invent.kde.org/vandenoever/kde-nix-flake.git
cd kde-nix-flake
scripts/update_packages.py --fetch --invent-dir ../invent
```
Running this script for the first time can take a while.
It will clone a large number of KDE repositories into the `../invent` folder.

The script updates the `packages.json` and `versions.json` files.
New versions of the software may require changes in the packaging.

Currently all version numbers in the packages are fixed to 0.1.
Code will be written to have more realistic version numbers.

## Updating `failed.json`

Packages that do not build are disabled by being listed in the file `failed.json`.
This file can be updated by running `scripts/build_packages`.
This will try to build _all_ packages.
Packages that fail to build are written to `failed.json`.

## Steps for updating packages

- scan `repo-metadata/projects-invent` for all the projects
- fetch all relevant repositories from <https://invent.kde.org>
- read all `metainfo.yaml` and `.kde-ci.yml` files
- write out `packages.json` and `versions.json`

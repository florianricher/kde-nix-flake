{ lib, pkgs, qt, kde, propagate, isQt6, ... }: {
  elisa = {
    buildInputs = [ qt.qtmultimedia qt.qt5compat ]
      ++ lib.optional (!isQt6) qt.qtquickcontrols2;
  };
}

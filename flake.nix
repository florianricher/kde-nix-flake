{
  description = "KDE software in a flake";
  inputs = { utils = { url = "github:numtide/flake-utils"; }; };
  outputs = { self, nixpkgs, utils }:
    let
      systems = [ "x86_64-linux" "aarch64-linux" ];
      packages_json = (builtins.fromJSON (builtins.readFile ./packages.json));
      versions_json = (builtins.fromJSON (builtins.readFile ./versions.json));
      failing_json = (builtins.fromJSON (builtins.readFile ./failing.json));
      gears_json = (builtins.fromJSON (builtins.readFile ./gears.json));
      layers = builtins.attrNames versions_json;
      mkPkgs = system:
        let
          vanilla_pkgs = import nixpkgs { inherit system; };
          pkgs = vanilla_pkgs // {
            wayland-protocols = vanilla_pkgs.wayland-protocols.overrideAttrs
              (old: {
                version = "1.32";
                src = pkgs.fetchurl {
                  url =
                    "https://gitlab.freedesktop.org/wayland/wayland-protocols/-/releases/1.32/downloads/wayland-protocols-1.32.tar.xz";
                  hash = "sha256-dFl5nTQMgpa2le+FfAfd7yTFoJsJq2p097kmQNKxuhE=";
                };
              });
          };
        in pkgs;
      mkLayer = system: layer:
        let
          vanilla_pkgs = import nixpkgs { inherit system; };
          pkgs = mkPkgs system;
          lib = pkgs.lib;
          isQt6 = lib.hasInfix "qt6" layer;
          qt = if isQt6 then
            pkgs.qt6
          else
            (pkgs.libsForQt5.qt5 // {
              # Define this in libsForQt5 so it can be included in qt5
              # without side effects.
              qt5compat = pkgs.libsForQt5.qt5.qtbase;
            });
          # combine the maps from versions.json and package.json
          data = builtins.mapAttrs
            (name: value: value // versions_json.${layer}.${name})
            packages_json.${layer};
          packages = lib.filterAttrs (name: value:
            lib.isDerivation value && !(lib.elem name failing_json.${layer}))
            (import ./packages.nix { inherit lib pkgs qt isQt6 data; });
          all = pkgs.buildEnv {
            name = layer;
            paths = builtins.attrValues packages;
          };
          mkGroup = tier: type:
            let
              attrs = lib.filterAttrs
                (name: value: value.tier or "" == tier && value.type == type)
                packages_json.${layer};
              names = builtins.attrNames attrs;
              old = map (name: {
                name = name;
                value = packages.${name};
              }) names;
            in lib.filterAttrs (name: value: builtins.elem name names) packages;
          mkTier = tier: {
            functional = mkGroup tier "functional";
            integration = mkGroup tier "integration";
            solution = mkGroup tier "solution";
          };
        in {
          packages = packages // all;
          groups = {
            tier1 = mkTier 1;
            tier2 = mkTier 2;
            tier3 = mkTier 3;
            tier4 = mkTier 4;
            pim = lib.filterAttrs (name: value:
              packages_json.${layer} ? ${name}
              && lib.hasPrefix "pim/" packages_json.${layer}.${name}.repopath)
              packages;
            plasma = lib.filterAttrs (name: value:
              packages_json.${layer} ? ${name} && lib.hasPrefix "plasma/"
              packages_json.${layer}.${name}.repopath) packages;
            gears = lib.filterAttrs (name: value: builtins.elem name gears_json)
              packages;
          };
        };
    in utils.lib.eachSystem systems (system:
      let
        layerAttrs = builtins.listToAttrs (map (layer: {
          name = layer;
          value = mkLayer system layer;
        }) layers);
        layerPackages =
          builtins.mapAttrs (name: value: value.packages) layerAttrs;
        layerGroups = builtins.mapAttrs (name: value: value.groups) layerAttrs;
        nativeBuildInputs = with pkgs.python3Packages;
          [
            python
            black
            gitpython
            isort
            mypy
            pydantic
            pylint
            pyyaml
            regex
            requests
            types-pyyaml
            types-requests
          ] ++ (with pkgs; [
            nix-prefetch-git
            nixfmt
            shellcheck
            cmake
            extra-cmake-modules
          ]);
        pkgs = mkPkgs system;
        packages = layerPackages // {
          default = layerPackages.stable-kf5-qt5;
          devEnv = pkgs.buildEnv {
            name = "devEnv";
            paths = nativeBuildInputs;
          };
        };
      in {
        inherit packages;
        hydraJobs = if system == "x86_64-linux" then layerGroups else { };
        # `nix develop`
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = nativeBuildInputs ++ (with pkgs.python3Packages; [
            python-lsp-server
            rope
            pkgs.rnix-lsp
          ]);
        };
      }) // (let
        # build a vm with
        #   nixos-rebuild build-vm --flake .#test-x86_64-linux
        mkConfiguration = system: pkgsToData:
          nixpkgs.lib.nixosSystem {
            inherit system;
            modules = [
              ({ config, pkgs, ... }: {
                imports = [
                  (builtins.toPath
                    "${nixpkgs}/nixos/modules/virtualisation/qemu-vm.nix")
                  (import ./module/plasma.nix (pkgsToData pkgs))
                ];
                virtualisation = {
                  memorySize = 4096;
                  cores = 3;
                  graphics = true;
                  qemu.guestAgent.enable = true;
                };
              })
              (builtins.toPath
                "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix")
              ({ config, pkgs, ... }: {
                config = rec {
                  system.stateVersion = "23.05";
                  nix.extraOptions = ''
                    experimental-features = nix-command flakes
                  '';
                  users.users = {
                    tester = {
                      isNormalUser = true;
                      extraGroups = [
                        "wheel"
                        "networkmanager"
                        "audio"
                        "video"
                        "weston-launcher"
                      ];
                      password = "test";
                      group = "users";
                    };
                  };
                  services.xserver = {
                    enable = true;
                    videoDrivers = [ "qxl" "modesetting" "fbdev" ];
                    displayManager = {
                      sddm.enable = true;
                      sessionPackages = [ pkgs.weston ];
                    };
                    desktopManager = {
                      plasma5flake = {
                        enable = true;
                        useQtScaling = true;
                      };
                      xterm.enable = true;
                    };
                  };
                  environment.systemPackages = with pkgs; [
                    clinfo
                    mesa-demos
                    vulkan-tools
                    wayland-utils
                    # for development and debugging
                    gdb
                    neovim
                    binutils
                    file
                    lsof
                    weston
                  ];
                };
              })
            ];
          };
      in {
        nixosConfigurations = {
          nixpkgs = mkConfiguration "x86_64-linux" (pkgs:
            (with pkgs.libsForQt5; {
              libsForQt = pkgs.libsForQt5;
              kde = kdeGear // kdeFrameworks // plasma5 // {
                breeze = breeze-qt5;
                polkit-kde-agent-1 = polkit-kde-agent;
              };
              isQt6 = false;
            }));
          stable-kf5-qt5 = mkConfiguration "x86_64-linux" (pkgs: {
            libsForQt = pkgs.libsForQt5;
            kde = (mkLayer "x86_64-linux" "stable-kf5-qt5").packages;
            isQt6 = false;
          });
          kf5-qt5 = mkConfiguration "x86_64-linux" (pkgs: {
            libsForQt = pkgs.libsForQt5;
            kde = (mkLayer "x86_64-linux" "kf5-qt5").packages;
            isQt6 = false;
          });
          kf6-qt6 = mkConfiguration "x86_64-linux" (pkgs: {
            libsForQt = pkgs.libsForQt5;
            kde = (mkLayer "x86_64-linux" "kf6-qt6").packages;
            isQt6 = true;
          });
        };
      });
  nixConfig = {
    extra-substituters = "https://bc.kde.broncode.org";
    extra-trusted-public-keys =
      "binarycache.nlnet.nl-1:YlgWTtAzszTbmwsq4BQQVVQ2lD/Ibo7WCStQaIYP3Sk=";
  };
}

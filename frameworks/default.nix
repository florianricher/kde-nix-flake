{ lib, pkgs, qt, kde, propagate, isQt6, ... }: {
  baloo = {
    buildInputs = [ pkgs.lmdb qt.qtdeclarative ];
    outputs = [ "out" "dev" ];

    # kde-baloo.service uses `ExecCondition=@KDE_INSTALL_FULL_BINDIR@/kde-systemd-start-condition ...`
    # which comes from the "plasma-workspace" derivation, but KDE_INSTALL_* all point at the "baloo" one
    # (`${lib.getBin pkgs.plasma-workspace}` would cause infinite recursion)
    postUnpack = ''
      substituteInPlace "$sourceRoot"/src/file/kde-baloo.service.in \
        --replace @KDE_INSTALL_FULL_BINDIR@ /run/current-system/sw/bin
    '';
  };
  bluez-qt = {
    preConfigure = ''
      substituteInPlace CMakeLists.txt \
        --replace /lib/udev/rules.d "$out/lib/udev/rules.d"
    '';
  };
  breeze-icons = {
    nativeBuildInputs = with pkgs; [ gtk3 python3 ];
    buildInputs = [ qt.qtsvg ];
    propagatedBuildInputs = with pkgs; [ hicolor-icon-theme ];
    dontDropIconThemeCache = true;
    outputs = [ "out" ]; # only runtime outputs
    postInstall = ''
      gtk-update-icon-cache "''${out:?}/share/icons/breeze"
      gtk-update-icon-cache "''${out:?}/share/icons/breeze-dark"
    '';
  };
  extra-cmake-modules = {
    patches = [
      # https://invent.kde.org/frameworks/extra-cmake-modules/-/merge_requests/268
      (pkgs.fetchpatch {
        url =
          "https://invent.kde.org/frameworks/extra-cmake-modules/-/commit/5862a6f5b5cd7ed5a7ce2af01e44747c36318220.patch";
        sha256 = "10y36fc3hnpmcsmjgfxn1rp4chj5yrhgghj7m8gbmcai1q5jr0xj";
      })
    ] ++ (if isQt6 then
      [ ./extra-cmake-modules/find_qml_module.patch ]
    else
      [ ./extra-cmake-modules/set_qt_plugin_path.patch ]);
    outputs = [ "out" ]; # this package has no runtime components
    propagatedBuildInputs = with pkgs; [ cmake pkg-config ];
    setupHook = ./extra-cmake-modules/setup-hook.sh;
  };
  kactivities = { buildInputs = [ pkgs.boost qt.qtdeclarative ]; };
  karchive = { buildInputs = with pkgs; [ bzip2 xz zlib zstd ]; };
  kauth = {
    patches = if isQt6 then
      [ ./kauth/cmake-install-paths.patch ]
    else
      [ ./kauth/cmake-install-paths.qt5.patch ];
    # library stores reference to plugin path,
    # separating $out from $bin would create a reference cycle
    outputs = [ "out" "dev" ];
    setupHook = propagate "out";
  };
  kcalendarcore = { buildInputs = [ pkgs.libical ]; };
  kcrash = { buildInputs = lib.optional (!isQt6) qt.qtx11extras; };
  kcodecs = { buildInputs = [ pkgs.gperf ]; };
  kcoreaddons = {
    buildInputs = [ pkgs.shared-mime-info ];
    dontWrapQtApps = true;
    # cmakeFlags = [ "-DEXCLUDE_DEPRECATED_BEFORE_AND_AT=CURRENT" ];
    postInstall = ''
      moveToOutput "mkspecs" "$dev"
    '';
  };
  kdelibs4support = {
    patches = [ ./kdelibs4support/nix-kde-include-dir.patch ];
    setupHook = ./kdelibs4support/setup-hook.sh;
    buildInputs = [ pkgs.networkmanager pkgs.xorg.libSM ]
      ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  kdoctools = {
    nativeBuildInputs = with pkgs; [
      # The build system insists on having native Perl.
      perl
      perlPackages.URI
    ];
    propagatedBuildInputs = with pkgs; [
      # kdoctools at runtime actually needs Perl for the platform kdoctools is
      # running on, not necessarily native perl.
      perl
      perlPackages.URI
    ];
    # buildInputs = [ karchive ki18n ];
    patches = [ ./kdoctools/kdoctools-no-find-docbook-xml.patch ];
    cmakeFlags = [
      "-DDocBookXML4_DTD_DIR=${pkgs.docbook_xml_dtd_45}/xml/dtd/docbook"
      "-DDocBookXSL_DIR=${pkgs.docbook_xsl_ns}/xml/xsl/docbook"
    ];
    postFixup = ''
      moveToOutput "share/doc" "$dev"
      moveToOutput "share/man" "$dev"
    '';
  };
  kemoticons = { outputs = [ "out" ]; };
  kfilemetadata = {
    buildInputs = lib.optionals pkgs.stdenv.isLinux [ pkgs.attr ]
      ++ (with pkgs; [ ebook_tools exiv2 ffmpeg kde.poppler taglib ])
      ++ [ qt.qtmultimedia kde.kdegraphics-mobipocket ];
    patches = if isQt6 then
      [ ./kfilemetadata/cmake-install-paths.patch ]
    else
      [ ./kfilemetadata/cmake-install-paths.qt5.patch ];
  };
  kguiaddons = {
    buildInputs = [ pkgs.wayland ] ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  kglobalaccel = {
    buildInputs = [ pkgs.xorg.libXdmcp ]
      ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  # only present in kf5
  khtml = {
    buildInputs = [ pkgs.gperf pkgs.giflib ]
      ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  ki18n = {
    propagatedNativeBuildInputs = with pkgs; [ gettext python3 ];
    buildInputs = [ qt.qtdeclarative ];
  };
  kiconthemes = {
    patches = [ ./kiconthemes/default-theme-breeze.patch ];
    buildInputs = [ qt.qtsvg ];
  };
  kidletime = {
    buildInputs =
      [ pkgs.wayland pkgs.wayland-protocols pkgs.wayland-scanner qt.qtwayland ]
      ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  kinit = { outputs = [ "out" ]; };
  kio = {
    buildInputs = [ qt.qt5compat pkgs.libkrb5 ]
      ++ lib.lists.optionals pkgs.stdenv.isLinux (with pkgs; [
        acl
        attr # both are needed for ACL support
        util-linux # provides libmount
      ]) ++ (lib.optional (!isQt6) qt.qtx11extras);
    patches = [ ./kio/0001-Remove-impure-smbd-search-path.patch ];
  };
  kirigami = {
    buildInputs = with qt;
      [ qttranslations qtsvg qt5compat ]
      ++ lib.optional (!isQt6) qt.qtquickcontrols2
      ++ lib.optional (!isQt6) qt.qtgraphicaleffects;
  };
  kjobwidgets = { buildInputs = lib.optional (!isQt6) qt.qtx11extras; };
  kjs = { buildInputs = [ pkgs.pcre ]; };
  knewstuff = {
    patches = if isQt6 then
      [ ./knewstuff/0001-Delay-resolving-knsrcdir.patch ]
    else
      [ ./knewstuff/0001-Delay-resolving-knsrcdir.qt5.patch ];
  };
  knotifications = {
    buildInputs = [ pkgs.libcanberra_kde pkgs.libdbusmenu kde.phonon ]
      ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  kquickcharts = {
    nativeBuildInputs = [ qt.qtdeclarative ]
      ++ lib.optional (!isQt6) qt.qtquickcontrols2;
  };
  kross = { buildInputs = lib.optional (!isQt6) qt.qtscript; };
  kservice = {
    # cmakeFlags = [ "-DEXCLUDE_DEPRECATED_BEFORE_AND_AT=CURRENT" ];
    buildInputs = [ ]
      ++ lib.optional (!isQt6) [ qt.qtx11extras pkgs.flex pkgs.bison ];
  };
  ksvg = { nativeBuildInputs = [ qt.qtdeclarative qt.qtsvg ]; };
  kxmlrpcclient = { outputs = [ "out" ]; };
  kwallet = { buildInputs = with pkgs; [ libgcrypt kde.gpgme ]; };
  kwayland = {
    buildInputs = [ pkgs.wayland pkgs.wayland-protocols ];
    setupHook = propagate "bin"; # XDG_CONFIG_DIRS
  };
  kwindowsystem = {
    buildInputs = [ pkgs.xorg.libXdmcp ]
      ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  modemmanager-qt = { propagatedBuildInputs = [ pkgs.modemmanager ]; };
  networkmanager-qt = { propagatedBuildInputs = [ pkgs.networkmanager ]; };
  oxygen-icons5 = {
    meta.license = lib.licenses.lgpl3Plus;
    outputs = [ "out" ]; # only runtime outputs
  };
  prison = {
    buildInputs = with pkgs; [ libdmtx qrencode zxing-cpp ];
    propagatedBuildInputs = [ qt.qtmultimedia ];
  };
  purpose = { buildInputs = [ kde.kdeclarative kde.prison ]; };
  qqc2-desktop-style = {
    buildInputs = lib.optional (!isQt6) qt.qtquickcontrols2;
  };
  solid = {
    nativeBuildInputs = with pkgs; [ bison flex media-player-info ];
    buildInputs = with qt; [ qtdeclarative ];
    propagatedUserEnvPkgs = [ pkgs.media-player-info ];
  };
  sonnet = { buildInputs = [ pkgs.aspell ]; };
  syntax-highlighting = { nativeBuildInputs = [ pkgs.perl ]; };
}

{ lib, pkgs, qt, isQt6, kde, ... }: {
  kaccounts-integration = {
    buildInputs = [
      pkgs.libsForQt5.qcoro
      pkgs.libsForQt5.signond
      pkgs.libsForQt5.accounts-qt
    ];
  };
  kaccounts-providers = {
    nativeBuildInputs = [ pkgs.intltool ];
    buildInputs = if isQt6 then
      [ # qcoro
      ]
    else [
      pkgs.libsForQt5.accounts-qt
      pkgs.libsForQt5.signond
    ];
  };
  kio-extras = {
    buildInputs = if isQt6 then [ qt.qtsvg qt.qt5compat ] else [ ];
  };
  neochat = {
    buildInputs = with pkgs;
      [ cmake cmark olm qt.qtmultimedia qt.qtlocation ]
      ++ (with kde; [ libquotient qtkeychain qcoro ])
      ++ (if isQt6 then [ qt.qt5compat ] else [ qt.qtquickcontrols2 ]);
  };
}

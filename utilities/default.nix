{ lib, pkgs, qt, kde, isQt6, ... }: {
  ark = { buildInputs = [ pkgs.libarchive ]; };
  filelight = {
    buildInputs =
      #with kde; [ kcoreaddons kirigami kquickcharts ] ++
      lib.optional (!isQt6) qt.qtquickcontrols2;
  };
  kate = {
    buildInputs = with qt;
      [ qt5compat ] ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  kcalc = { buildInputs = with pkgs; [ qt.qt5compat gmp mpfr ]; };
  kclock = {
    buildInputs = [ qt.qtmultimedia ]
      ++ lib.optional (!isQt6) qt.qtquickcontrols2;
  };
  kirogi = {
    buildInputs = with qt;
      [ qtlocation qtpositioning ] ++ lib.optional (!isQt6) qt.qtgamepad;
  };
  konsole = {
    buildInputs = with qt; [ qt5compat qtmultimedia ];
    # passthru.tests.test = nixosTests.terminal-emulators.konsole;
  };
  print-manager = { buildInputs = [ pkgs.cups qt.qt5compat ]; };
}

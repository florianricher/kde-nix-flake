{ lib, pkgs, qt, kde, isQt6, ... }: {
  akonadi = {
    buildInputs = [
      qt.qt5compat
      pkgs.libsForQt5.accounts-qt
      pkgs.libsForQt5.signond
      pkgs.shared-mime-info
      pkgs.xz
    ];
  };
  akonadi-calendar = { buildInputs = [ kde.grantlee ]; };
  akonadiclient = { buildInputs = [ kde.grantlee kde.akonadi ]; };
  akonadiconsole = {
    buildInputs = [ pkgs.xapian qt.qtwebengine kde.grantlee ];
  };
  akonadi-contacts = { buildInputs = [ kde.grantlee ]; };
  akonadi-import-wizard = {
    buildInputs = [ qt.qtwebengine kde.grantlee kde.qtkeychain ];
  };
  akonadi-mime = { buildInputs = [ pkgs.shared-mime-info ]; };
  akonadi-search = { buildInputs = [ pkgs.xapian ]; };
  akregator = { buildInputs = [ qt.qtwebengine kde.grantlee ]; };
  grantlee-editor = { buildInputs = [ qt.qtwebengine kde.grantlee ]; };
  grantleetheme = { buildInputs = [ kde.grantlee ]; };
  incidenceeditor = { buildInputs = [ kde.grantlee ]; };
  itinerary = {
    buildInputs = [ pkgs.shared-mime-info qt.qtpositioning ]
      ++ lib.optional (!isQt6) qt.qtquickcontrols2;
  };
  kaddressbook = { buildInputs = [ kde.grantlee ]; };
  kalarm = {
    buildInputs = [ kde.grantlee ] ++ lib.optional (!isQt6) qt.qtx11extras;
  };
  kcalutils = { buildInputs = [ kde.grantlee ]; };
  kdav2 = { buildInputs = lib.optional (!isQt6) qt.qtxmlpatterns; };
  kdepim-addons = { buildInputs = [ qt.qtwebengine kde.grantlee ]; };
  kdepim-runtime = {
    buildInputs =
      [ pkgs.shared-mime-info qt.qtnetworkauth kde.grantlee kde.qtkeychain ];
  };
  kimap = { propagatedBuildInputs = [ pkgs.cyrus_sasl ]; };
  kimap2 = { propagatedBuildInputs = [ pkgs.cyrus_sasl ]; };
  kitinerary = {
    buildInputs = [ pkgs.shared-mime-info pkgs.zxing-cpp kde.poppler ];
  };
  kitinerary-workbench = { buildInputs = [ qt.qt5compat ]; };
  kjots = { buildInputs = [ kde.grantlee ]; };
  kldap = {
    buildInputs = [ kde.qtkeychain ];
    propagatedBuildInputs = [ pkgs.cyrus_sasl pkgs.openldap ];
  };
  kmail = { buildInputs = [ qt.qtwebengine kde.grantlee kde.qtkeychain ]; };
  kmail-account-wizard = {
    buildInputs = [ pkgs.shared-mime-info ]
      ++ lib.optional (!isQt6) qt.qtscript;
  };
  kmailtransport = { buildInputs = [ kde.qtkeychain ]; };
  kmime = { buildInputs = [ qt.qt5compat ]; };
  knotes = { buildInputs = [ kde.grantlee ]; };
  kontact = { buildInputs = [ qt.qtwebengine kde.grantlee ]; };
  kontactinterface = { buildInputs = lib.optional (!isQt6) qt.qtx11extras; };
  korganizer = { buildInputs = [ kde.grantlee ]; };
  kpkpass = { buildInputs = [ pkgs.shared-mime-info ]; };
  ksmtp = { propagatedBuildInputs = [ pkgs.cyrus_sasl ]; };
  libkgapi = { buildInputs = [ pkgs.cyrus_sasl ]; };
  libkleo = { propagatedBuildInputs = [ kde.gpgme pkgs.boost qt.qt5compat ]; };
  libksieve = { buildInputs = [ qt.qtwebengine ]; };
  mailcommon = { buildInputs = [ qt.qtwebengine kde.grantlee ]; };
  mbox-importer = { buildInputs = [ kde.grantlee ]; };
  mimetreeparser = { buildInputs = [ kde.gpgme qt.qt5compat ]; };
  messagelib = { buildInputs = [ qt.qtwebengine kde.grantlee ]; };
  pimcommon = { buildInputs = [ kde.grantlee ]; };
  pim-data-exporter = { buildInputs = [ kde.grantlee ]; };
  pim-sieve-editor = { buildInputs = [ kde.qtkeychain ]; };
  sink = {
    buildInputs =
      [ pkgs.flatbuffers pkgs.lmdb pkgs.xapian kde.gpgme pkgs.zstd pkgs.curl ];
  };
  vakzination = { buildInputs = lib.optional (!isQt6) qt.qtquickcontrols2; };
}

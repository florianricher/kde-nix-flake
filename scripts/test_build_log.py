#!/usr/bin/env python
import argparse
import sys

from modules.nix_log_parser import ParseError, Parser


def main() -> None:
    argparser = argparse.ArgumentParser(
        description="Test parsing of an output file for debugging.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    argparser.add_argument(
        "input_log",
        help="The name of the file to parse",
    )
    args = argparser.parse_args()
    parser = Parser()
    lineno = 0
    with open(args.input_log, "r", encoding="utf-8") as file:
        for line in file:
            if line.endswith("\n"):
                line = line[:-1]
            lineno += 1
            try:
                parser.parse(line)
            except ParseError as e:
                print(f"Error parsing: {e}", file=sys.stderr)
                raise e
            # print(f"{lineno}\t{line}")
    log = parser.finish()
    failed = []
    for name, package in log.package_logs.items():
        if not package.ok:
            failed.append(name[33:])
    print(failed)


if __name__ == "__main__":
    main()

import re
from dataclasses import dataclass, field
from typing import Dict, List, Optional, Set

from pydantic import BaseModel


class PackageLog(BaseModel):
    """The build log for one package."""

    log: List[str] = []
    ok: bool = False
    started: bool = False
    short_names: Set[str] = set()


class BuildLog(BaseModel):
    """The parsed log of `nix build`"""

    package_logs: Dict[str, PackageLog] = {}
    fetched_paths: List[str] = []
    errors: List[str] = []


@dataclass
class Header:
    done: bool = False
    build_count: Optional[int] = None
    build_lines: int = 0
    fetch_count: Optional[int] = None


# The short name matcher culls the package name down to the label use for
# writing build logs for a package. This label, or short_name, is used in lines
# like 'short_name> -- Detecting CXX compiler ABI info - done'
# The short name matcher is found by trial and error, not by reading the nix
# source code.
short_name_matcher = re.compile(r".*?(-+\d+[\w.+-]*)")
hex_name_matcher = re.compile(r".*-[0-9a-f]{7}$")


class ParseError(Exception):
    pass


@dataclass
class Parser:
    """dict with package name-version as key and a name and version tuple as value"""

    header: Header = field(default_factory=Header)
    line_number: int = 0
    log: BuildLog = BuildLog()
    in_error_log: bool = False
    running_builds: Dict[str, str] = field(default_factory=dict)
    lines: List[str] = field(default_factory=list)

    def parse(self, line: str) -> None:
        self.lines.append(line)
        handle_line(self, line)

    def announce_package(self, package: str) -> None:
        self.log.package_logs[package] = PackageLog()

    def start_log(self, rev_hash: str, package: str) -> None:
        full_package = f"{rev_hash}-{package}"
        self.log.package_logs[full_package].ok = True
        self.log.package_logs[full_package].started = True
        if match := hex_name_matcher.match(package):
            self.running_builds[package] = full_package
            short_name = package[:-8]
            self.running_builds[short_name] = full_package
        else:
            short_name = package
        match = short_name_matcher.match(short_name)
        if match:
            short_name = short_name[: -len(match[1])]
            self.running_builds[short_name] = full_package

    def package_log(self, short_name: str, msg: str) -> None:
        if short_name not in self.running_builds:
            self.log.errors.append(
                f"No package {short_name} found at line '{short_name}> {msg}'"
            )
            return
        package = self.running_builds[short_name]
        self.log.package_logs[package].log.append(msg)
        self.log.package_logs[package].short_names.add(short_name)

    def failed_dependencies(self, package: str) -> None:
        print(f"deps failed for {package}")
        self.log.package_logs[package].ok = False

    def hash_mismatch(self, package: str) -> None:
        print(f"hash mismatch for {package}")
        self.log.package_logs[package].ok = False

    def build_error(self, package: str) -> None:
        print(f"build error for {package}")
        self.log.package_logs[package].ok = False

    def finish(self) -> BuildLog:
        return finish(self)

    def save_log(self, filename: str) -> None:
        """Write the nix log that was gathered so far to a file"""
        with open(filename, "w", encoding="utf-8") as file:
            for line in self.lines:
                print(line, file=file)


fetch_matcher = re.compile(r"these (\d+) paths will be fetched \(.*\):")
path_matcher = re.compile(r"  /nix/store/(\w{32})-([\w.+-]+)")


def handle_fetch_line(parser: Parser, line: str) -> bool:
    if parser.header.fetch_count is None:
        if match := fetch_matcher.fullmatch(line):
            parser.header.fetch_count = int(match[1])
            return True
    elif len(parser.log.fetched_paths) < parser.header.fetch_count:
        if match := path_matcher.fullmatch(line):
            parser.log.fetched_paths.append(match[2])
            return True
        raise ParseError(
            f"Could only parse {len(parser.log.fetched_paths)} out of "
            + f"{parser.header.fetch_count} fetched paths: "
            + f'"{line}"'
        )
    return False


build_matcher = re.compile(r"these (\d+) derivations will be built:")
derivation_matcher = re.compile(r"  /nix/store/(\w{32})-([\w.+-]+)\.drv")


def handle_build_line(parser: Parser, line: str) -> bool:
    if parser.header.build_count is None:
        if match := build_matcher.fullmatch(line):
            parser.header.build_count = int(match[1])
            return True
        if line == "this derivation will be built:":
            parser.header.build_count = 1
            return True
    elif parser.header.build_lines < parser.header.build_count:
        if match := derivation_matcher.fullmatch(line):
            parser.header.build_lines += 1
            parser.announce_package(f"{match[1]}-{match[2]}")
            return True
        raise ParseError(
            f"Could only parse {parser.header.build_lines} out of "
            + f"{parser.header.build_count} fetched paths: "
            + f'"{line}"'
        )
    return False


def handle_header_line(parser: Parser, line: str) -> bool:
    """Return True if the line was handled"""
    if parser.header.done:
        return False
    if warning_matcher.fullmatch(line):
        return True
    if handle_build_line(parser, line):
        return True
    if handle_fetch_line(parser, line):
        return True
    parser.header.done = True
    return False


warning_matcher = re.compile(r"warning: (.*)")
prefixed_line_matcher = re.compile(r"([\w+-]+)> (.*)")
error_builder_matcher = re.compile(
    r"error: builder for '/nix/store/(\w{32}-[\w.+-]+).*\.drv'(.*)"
)
build_start_matcher = re.compile(
    r"building '/nix/store/(\w{32})-([\w.+-]+)\.drv'\.\.\."
)
failed_dependencies_matcher = re.compile(
    r"error: \d+ dependencies of derivation '/nix/store/(\w{32}-[\w.+-]+)\.drv' failed to build"
)
hash_mismatch_matcher = re.compile(
    r"error: hash mismatch in fixed-output derivation '/nix/store/(\w{32}-[\w.+-]+)\.drv':"
)
copying_matcher = re.compile(
    r"copying path '/nix/store/(\w{32})-([^']+)' from '[^']+'\.\.\."
)
# use this afer error_builder_matcher
# builder_error_matcher = re.compile("failed to produce output path for output '"
#     + r"(\w_)' at '/nix/store/(\w{32})-(.*)\.drv\.(.*)/nix/store/(\w{32})-(.*)")


def handle_line(parser: Parser, line: str) -> None:
    parser.line_number += 1
    if match := copying_matcher.match(line):
        return
    if (
        warning_matcher.fullmatch(line)
        # these two lines are possible messages when `nix build` starts that
        # can be ignored.
        or line.startswith("Using saved setting for ")
        or line == "Pass '--accept-flake-config' to trust it"
        or handle_header_line(parser, line)
    ):
        return
    if parser.in_error_log:
        if line.startswith("       "):
            # still in error log
            return
        parser.in_error_log = False
    if match := build_start_matcher.fullmatch(line):
        parser.start_log(match[1], match[2])
        return

    if match := prefixed_line_matcher.match(line):
        package = match[1]
        log_line = match[2]
        parser.package_log(package, log_line)
    elif match := error_builder_matcher.match(line):
        parser.build_error(match[1])
        parser.in_error_log = True
    elif match := failed_dependencies_matcher.match(line):
        parser.failed_dependencies(match[1])
        return
    elif match := hash_mismatch_matcher.match(line):
        parser.hash_mismatch(match[1])
        parser.in_error_log = True
        return
    else:
        print(parser.header)
        print(len(parser.log.package_logs))
        raise ParseError(f'Could not match line {parser.line_number}: "{line}"')


def finish(parser: Parser) -> BuildLog:
    double = 0
    for f in parser.log.fetched_paths:
        if f in parser.log.package_logs:
            double += 1
    ok = 0
    for p in parser.log.package_logs.values():
        if p.ok:
            ok += 1
    print(f"{len(parser.log.package_logs)}\t{len(parser.log.fetched_paths)}\t{double}")
    print(f"ok: {ok}, failed: {len(parser.log.package_logs)-ok}")
    return parser.log

#! /usr/bin/env python3
from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict, List, Optional, Union

from pydantic import BaseModel, Extra, Field


class Bugzilla(BaseModel):
    product: str
    legacy: Optional[str] = Field(None, alias="__do_not_use-legacy-product")
    component: Optional[str]

    class Config:
        extra = Extra.forbid


class Metadata(BaseModel):
    bugzilla: Optional[Bugzilla]
    description: str
    hasrepo: bool
    identifier: str
    name: str
    projectpath: str
    repoactive: bool
    repopath: str
    topics: List[str] = []


class Library(BaseModel):
    qmake: Optional[str]
    cmake: Optional[str]
    license: Optional[str]

    class Config:
        extra = Extra.forbid


class Platform(BaseModel):
    name: str
    note: Optional[str]

    class Config:
        extra = Extra.forbid


class Metainfo(BaseModel):
    cmakename: Optional[str]
    deprecated: bool = False
    description: str
    fancyname: Optional[str]
    framework_dependencies: Optional[List[str]] = Field(alias="framework-dependencies")
    group: Optional[str]
    group_info: Optional[Any]
    irc: Optional[str]
    libraries: Optional[Union[List[str], List[Library]]]
    logo: Optional[str]
    mailinglist: Optional[str]
    maintainer: Optional[Union[str, List[str]]]
    name: Optional[str]
    platforms: List[Platform] = []
    portingAid: bool = False
    public_doc_dir: Optional[Union[str, List[str]]]
    public_example_dir: Optional[str]
    public_example_dirs: Optional[Union[str, List[str]]]
    public_lib: bool = False
    public_source_dirs: Optional[Union[str, List[str]]]
    qdoc: bool = False
    release: bool = False
    subgroup: Optional[str]
    tier: Optional[int]
    type: Optional[str]

    class Config:
        extra = Extra.forbid


class Dependency(BaseModel):
    on: List[str]
    require: Dict[str, str]

    class Config:
        extra = Extra.forbid


class KdeCi(BaseModel):
    Dependencies: List[Dependency] = []
    RuntimeDependencies: List[Dependency] = []
    runtime_dependencies: Dict[str, str] = Field(
        alias="runtime-dependencies", default={}
    )
    Environment: Dict[str, str] = {}
    Options: Dict[str, Any] = {}
    PostInstallScripts: Dict[str, str] = {}

    class Config:
        extra = Extra.forbid


@dataclass
class BranchInfo:
    branch: str
    rev: str
    date: datetime
    sha256: str
    metainfo: Optional[Metainfo]
    kde_ci: Optional[KdeCi]
    version: str


@dataclass
class Info:
    branch: BranchInfo
    dependencies: List[str]

import json
from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List, Optional, Set, Tuple

from pydantic import BaseModel


@dataclass
class LogicalModuleStructure:
    version: int
    layers: Set[str]
    groups: Dict[str, Dict[str, str]]
    """List of tuples where the first entry of the tuple is a string that ends
       with '/' or an empty string.
       The prefix is derived from the group rule. A group rule that ends with
       '/*' becomes a prefix by removing the trailing '*'.
       If a projectpath starts with the prefix thus defined, it matches the
       given group."""
    prefixes: List[Tuple[str, Dict[str, str]]]
    default_branch: str

    def get_branch(self, layer: str, projectpath: str) -> Optional[str]:
        """Return the branch for the project in the given layer
        or None if the project is not part of the given layer.
        """
        group = None
        if projectpath in self.groups:
            group = self.groups[projectpath]
        else:
            for prefix in self.prefixes:
                if projectpath.startswith(prefix[0]):
                    group = prefix[1]
                    break
        if group and layer in group:
            if group[layer] == "":
                return None
            return group[layer]
        return self.default_branch


class _LMS(BaseModel):
    """The logical module structure.
    It is documented at
      http://community.kde.org/Infrastructure/Project_Metadata#kde-build-metadata
    """

    version: int
    layers: Set[str] = set()
    groups: Dict[str, Dict[str, str]] = {}


def _check_group_layers(lms: _LMS) -> None:
    error = ""
    for name, group in lms.groups.items():
        for layer in group:
            if layer not in lms.layers:
                error += f"{layer} is not a valid layer in {name}\n"
    if error:
        raise ValueError(error)


def _sort_prefixes(prefixes: List[Tuple[str, Dict[str, str]]]) -> None:
    prefixes.sort(key=lambda tuple: tuple[0].count("/"), reverse=True)


def _split_prefixes_from_names(
    groups: Dict[str, Dict[str, str]]
) -> Tuple[Dict[str, Dict[str, str]], List[Tuple[str, Dict[str, str]]]]:
    new_groups = {}
    prefixes = []
    for name, group in groups.items():
        if name.endswith("/*") or name == "*":
            prefixes.append((name[:-1], group))
        else:
            new_groups[name] = group
    _sort_prefixes(prefixes)
    return (new_groups, prefixes)


def get_logical_module_structure(
    path: Path, default_branch: str = "master"
) -> LogicalModuleStructure:
    filepath = Path(path, "dependencies/logical-module-structure")
    with open(filepath, "r", encoding="utf-8") as file:
        lms_json = json.load(file)
        lms = _LMS(**lms_json)
    _check_group_layers(lms)
    groups, prefixes = _split_prefixes_from_names(lms.groups)
    return LogicalModuleStructure(
        lms.version, lms.layers, groups, prefixes, default_branch
    )
